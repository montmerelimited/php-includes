#!/bin/bash
DIR="$( cd "$( dirname "$0" )" && pwd )"
mkdir -p "${DIR}/Doctrine"

rm -rf "${DIR}/Doctrine/Common"
ln -s "${DIR}/submodules/doctrine2-common/lib/Doctrine/Common" "${DIR}/Doctrine/Common"

rm -rf "${DIR}/Doctrine/DBAL"
ln -s "${DIR}/submodules/doctrine2-dbal/lib/Doctrine/DBAL" "${DIR}/Doctrine/DBAL"

rm -rf "${DIR}/Doctrine/ORM"
ln -s "${DIR}/submodules/doctrine2-orm/lib/Doctrine/ORM" "${DIR}/Doctrine/ORM"

rm -rf "${DIR}/DoctrineExtensions"
ln -s "${DIR}/submodules/DoctrineExtensions/lib/DoctrineExtensions" "${DIR}/DoctrineExtensions"

rm -rf "${DIR}/PHPThumb"
ln -s "${DIR}/submodules/PHPThumb/src" "${DIR}/PHPThumb"

rm -rf "${DIR}/Zym"
ln -s "${DIR}/submodules/zym/library/Zym" "${DIR}/Zym"

rm -rf "${DIR}/Zend"
ln -s "${DIR}/submodules/Zend/Zend" "${DIR}/Zend"

rm -rf "${DIR}/ZendX"
ln -s "${DIR}/submodules/Zend/ZendX" "${DIR}/ZendX"

rm -rf "${DIR}/ZFDebug"
ln -s "${DIR}/submodules/ZFDebug/library/ZFDebug" "${DIR}/ZFDebug"

rm -rf "${DIR}/M"
ln -s "${DIR}/submodules/the-m-library/library/M" "${DIR}/M"

rm -rf "${DIR}/GoogleCheckout"
ln -s "${DIR}/submodules/GoogleCheckout/library/GoogleCheckout" "${DIR}/GoogleCheckout"

rm -rf "${DIR}/Gedmo"
ln -s "${DIR}/submodules/Gedmo/lib/Gedmo" "${DIR}/Gedmo"

rm -rf "${DIR}/Aktive-Merchant"
ln -s "${DIR}/submodules/Aktive-Merchant/lib" "${DIR}/Aktive-Merchant"

chmod 777 -R "${DIR}/submodules/Aktive-Merchant/log"
