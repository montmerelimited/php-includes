#!/bin/bash
DIR="$( cd "$( dirname "$0" )" && pwd )"

# make the symlinks
bash "${DIR}/../symlinks.sh"

FILE="${HOME}/.zf.ini"
INCLUDE_PATH="${HOME}/repos/php-includes:/usr/share/php"

if [ -f $FILE ]; then
	echo "${FILE} exists, make sure your include path is set there properly"
	echo "Also, make sure your manifest M\\Tool\\Manifest is in basicloader.classes.0"
else
	echo "${FILE} does not exist, creating it with an include path of ${INCLUDE_PATH}"
	echo "php.include_path = \"${INCLUDE_PATH}\"" > "${FILE}"
	echo "basicloader.classes.0 = \"M\\Tool\\Manifest\"" >> "${FILE}"
fi
